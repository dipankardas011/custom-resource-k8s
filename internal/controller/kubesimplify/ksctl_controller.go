/*
Copyright 2023 dipankardas011.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubesimplify

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	kubesimplifyv1alpha1 "gitlab.com/dipankardas011/custom-resource-k8s.git/api/kubesimplify/v1alpha1"
)

// KsctlReconciler reconciles a Ksctl object
type KsctlReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=kubesimplify.core.dipankar.io,resources=ksctls,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kubesimplify.core.dipankar.io,resources=ksctls/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kubesimplify.core.dipankar.io,resources=ksctls/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Ksctl object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.4/pkg/reconcile
func (r *KsctlReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	l := log.FromContext(ctx)

	ksctl := &kubesimplifyv1alpha1.Ksctl{}

	err := r.Get(ctx, req.NamespacedName, ksctl)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if ksctl.Spec.Machine.HAControlPlaneNodes <= 0 {
		ksctl.Annotations = map[string]string{
			"Validate": "false",
		}
		if err := r.Delete(ctx, ksctl); err != nil {
			return ctrl.Result{}, err
		}

		l.Error(fmt.Errorf("invalid Machine"), "no of ha controlplane must be > 0")
		return ctrl.Result{}, fmt.Errorf("invalid Machine")
	}

	l.Info("Reconceling Machine", "Name", ksctl.Name, "Namespace", ksctl.Namespace)

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *KsctlReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&kubesimplifyv1alpha1.Ksctl{}).
		Complete(r)
}
