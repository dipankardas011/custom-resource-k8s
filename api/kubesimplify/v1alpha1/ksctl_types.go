/*
Copyright 2023 dipankardas011.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	ksctl "github.com/kubesimplify/ksctl/api/utils"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// KsctlSpec defines the desired state of Ksctl
type KsctlSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Machine ksctl.Machine `json:"machine"`
}

// KsctlStatus defines the observed state of Ksctl
type KsctlStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Ksctl is the Schema for the ksctls API
type Ksctl struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   KsctlSpec   `json:"spec,omitempty"`
	Status KsctlStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// KsctlList contains a list of Ksctl
type KsctlList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Ksctl `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Ksctl{}, &KsctlList{})
}
